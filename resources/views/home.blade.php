@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Posts</div>

                <div class="panel-body">
                @foreach($allposts as $post)

                    @if($post->type == 'youtube')
                     <p>
                         <h1>{{$post->title}}</h1>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$post->providerId}}" frameborder="0" allowfullscreen></iframe>
                            <br>
                        <a href="/posts/{{$post->id}}">Add comment</a>
                        </p>

                    @elseif($post->type=='vimeo')
                            <p>
                            <h1>{{$post->title}}</h1>
                            <iframe src="https://player.vimeo.com/video/{{$post->providerId}}?badge=0" width="560" height="280" frameborder="0" allowfullscreen></iframe>
                            <br>
                            <a href="/posts/{{$post->id}}">Add comment</a>
                            </p>
                        @elseif($post->type== 'sound')

                            <iframe width="100%" height="166" scrolling="no" frameborder="no"src="http://w.soundcloud.com/player/?url={{$post->url}}&auto_play=false&color=915f33&theme_color=00FF00"></iframe>
                            <br>
                            <a href="/posts/{{$post->id}}">Add comment</a>
                        @endif


               @endforeach
               <div class="pagination">
                            {!! $allposts->render() !!}

               </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
