@extends('layouts.app')

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Posts</h2></div>

                    <div class="panel-body">

                        @if($type == 'youtube')
                            <h1>{{$title}}</h1>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$providerId}}" frameborder="0" allowfullscreen></iframe>
                            </p>

                        @elseif($type=='vimeo')
                            <h1>{{$title}}</h1>
                            <iframe src="https://player.vimeo.com/video/{{$providerId}}?badge=0" width="560" height="280" frameborder="0" allowfullscreen></iframe>
                            <br>

                        @elseif($type== 'sound')

                            <iframe width="100%" height="166" scrolling="no" frameborder="no"src="http://w.soundcloud.com/player/?url={{$url}}&auto_play=false&color=915f33&theme_color=00FF00"></iframe>

                        @endif

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Comments</h2></div>

                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/comment') }}">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label class="col-md-4 control-label">Enter Comment</label>

                                <div class="col-md-6">
                                    <input id="{{$postid}}" name="postid" type="hidden" value="{{$postid}}">
                                    <input type="comment" class="form-control" name="comment" value="{{ old('comment') }}">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn"></i>Add comment
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    @for ($i = 0; $i < count($comment); $i++)
                                    <p>{{$comment[$i]->comment}}</p>
                                    @endfor

                                </div>

                            </div>


                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
