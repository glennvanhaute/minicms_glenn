<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        if (Auth::check()) {
            return redirect('/home');
        }
        return view('welcome');
    });
    Route::get('/add','addController@show');
    Route::post('/add','addController@addUrl');
    Route::get('posts/{id}', 'PostsController@postDetail');
    Route::post('/comment', 'CommentController@addComment');


});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
