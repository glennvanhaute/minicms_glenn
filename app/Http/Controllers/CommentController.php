<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Table_comments;
use Illuminate\Http\Request;
use App\Http\Requests\commentrequest;
use Illuminate\Support\Facades\Redirect;

use Input;
use DB;
use Carbon;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function show(){



        return view ("add");
    }

    public function addComment(commentrequest $request)
    {
        $sComment= $request->get("comment");
        $sId= $request->get("postid");

            $comment = new table_comments();
            $comment->postId = $sId;
            $comment->comment = $sComment;
            $comment->save();
        $redirecturl = '/posts/' . $sId;
        return Redirect::to($redirecturl);


    }

}