<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Table_posts;
use Illuminate\Http\Request;
use Input;
use DB;
use Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $allposts = DB::table('posts')->orderBy('created_at', 'desc')->paginate(5);
        return view('home', compact('allposts'));
    }
}
