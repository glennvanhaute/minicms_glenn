<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Table_posts;
use Illuminate\Http\Request;
use App\Http\Requests\urlrequest;
use Input;
use DB;
use Carbon;

class addController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function show(){



        return view ("add");
    }

    public function addUrl(urlrequest $request)
    {
        $sUrl= $request->get("url");
        $str = file_get_contents($sUrl);

        $parsed_url = parse_url($sUrl);

        if (strpos($sUrl,'youtube') !== false) {
            if (strlen($str) > 0) {
                $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                preg_match("/\<title\>(.*)\<\/title\>/i", $str, $title); // ignore case


                parse_str( parse_url( $sUrl, PHP_URL_QUERY ), $my_array_of_vars );
                $vidID = $my_array_of_vars['v'];

                $sTitle =$title[1];
                $post = new table_posts();
                $post->url = $sUrl;
                $post->title = $sTitle;
                $post->type = 'youtube';
                $post->providerId = $vidID;

                $post->save();
                return \Redirect::to("/home");
            }
        }
        if (strpos($sUrl,'vimeo') !== false) {
            if (strlen($str) > 0) {
                $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
                preg_match("/\<title\>(.*)\<\/title\>/i", $str, $title); // ignore case

                $urlParts = explode("/", parse_url($sUrl, PHP_URL_PATH));
                $vidID = (int)$urlParts[count($urlParts)-1];


                $sTitle =$title[1];
                $post = new table_posts();
                $post->url = $sUrl;
                $post->title = $sTitle;
                $post->type = 'vimeo';
                $post->providerId = $vidID;
                $post->save();
                return \Redirect::to("/home");
            }
        }
        if ($parsed_url['host'].contains('soundcloud')) {

                $post = new table_posts();
                $post->url = $parsed_url;
                $post->type = 'soundcloud';
                $post->save();
                return \Redirect::to("/home");

        }





    }

}