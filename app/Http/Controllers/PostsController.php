<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Table_posts;
use Illuminate\Http\Request;
use App\Http\Requests\urlrequest;
use Input;
use DB;
use Carbon;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function postDetail($id)
    {

        $project = \DB::table('posts')->where('id', $id)->select('id', 'title','providerId','type', 'created_at')->get();
        $comment = \DB::table('comments')->where('postId', $project[0]->id)->select('id', 'postId', 'comment')->paginate(5);


        $detail = [];
        $detail['postid'] = $project[0]->id;
        $detail['title'] = $project[0]->title;
        $detail['type'] = $project[0]->type;
        $detail['providerId'] = $project[0]->providerId;
        $detail['comment'] = $comment;


        return view('posts.detail', $detail);
    }





}